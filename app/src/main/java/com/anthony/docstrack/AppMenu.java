package com.anthony.docstrack;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.UserManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnMenuTabSelectedListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.R.attr.data;
import static android.R.id.button1;
import static com.anthony.docstrack.R.id.add_documents;
import static com.anthony.docstrack.R.id.dashboard;
import static com.anthony.docstrack.R.id.reports;
import static com.anthony.docstrack.R.id.textView;
import static com.anthony.docstrack.R.id.viewLabel;
import static com.anthony.docstrack.R.id.workflows;

public class AppMenu extends AppCompatActivity {

    private RelativeLayout relativeLayout;

    public TextView viewLabel;

    public ViewSwitcher dashboardViewSwitcher;
    public ListView projectsView;
    public ListView documentsView;

    public Button dashboardBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_menu);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.docstracklogo);
        setTitle("DocsTrack");

        relativeLayout = (RelativeLayout) findViewById(R.id.activity_app_menu);

        viewLabel = (TextView) findViewById(R.id.viewLabel);

        projectsView = (ListView) findViewById(R.id.projectsView);
        documentsView = (ListView) findViewById(R.id.documentsView);

        dashboardBackButton = (Button) findViewById(R.id.dashboardBackButton);
        dashboardBackButton.setVisibility(View.INVISIBLE);

        // Initializing a new String Array
        String[] fruits = new String[] {
                "Cape Gooseberry",
                "Capuli cherry",
                "Cape Gooseberry",
                "Capuli cherry",
                "Cape Gooseberry",
                "Capuli cherry",
                "Cape Gooseberry",
                "Capuli cherry",
                "Cape Gooseberry",
                "Capuli cherry",
                "Cape Gooseberry",
                "Capuli cherry"
        };

        String[] colors = new String[] {
                "Red",
                "Red",
                "Red",
                "Red",
                "Red",
                "Red"
        };

        // Create a List from String Array elements
        final List<String> fruits_list = new ArrayList<String>(Arrays.asList(fruits));
        final List<String> colors_list = new ArrayList<String>(Arrays.asList(colors));

        // Create an ArrayAdapter from List
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, fruits_list);
        final ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, colors_list);

        // DataBind ListView with items from ArrayAdapter
        projectsView.setAdapter(arrayAdapter);
        documentsView.setAdapter(arrayAdapter1);

        dashboardViewSwitcher = (ViewSwitcher)findViewById(R.id.dashboardViewSwitcher);
        projectsView = (ListView)findViewById(R.id.projectsView);
        documentsView = (ListView)findViewById(R.id.documentsView);

        projectsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (dashboardViewSwitcher.getCurrentView() != projectsView){
                    dashboardViewSwitcher.showPrevious();
                } else if (dashboardViewSwitcher.getCurrentView() != documentsView) {
                    dashboardViewSwitcher.showNext();
                    viewLabel.setText("DOCUMENTS");
                    dashboardBackButton.setVisibility(View.VISIBLE);
                }
            }
        });

        dashboardBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dashboardViewSwitcher.getCurrentView() == documentsView) {
                    dashboardViewSwitcher.showPrevious();
                    dashboardBackButton.setVisibility(View.INVISIBLE);
                    viewLabel.setText("PROJECTS");
                }
            }
        });

        // Click Handling for BottomBar Items
        final BottomBar bottomBar = BottomBar.attach(this, savedInstanceState);
        bottomBar.setItemsFromMenu(R.menu.tabs_bar, new OnMenuTabSelectedListener() {
            @Override
            public void onMenuItemSelected(int itemId) {
                if(itemId == R.id.dashboard) {
                    Toast.makeText(getApplicationContext(),"Dashboard tapped", Toast.LENGTH_SHORT).show();
                    dashboardViewSwitcher.setVisibility(View.VISIBLE);
                } else if(itemId == R.id.add_documents) {
                    dashboardViewSwitcher.setVisibility(View.INVISIBLE);
                    viewLabel.setText("ADD DOCMENTS");
                } else if(itemId == R.id.workflows) {
                    dashboardViewSwitcher.setVisibility(View.INVISIBLE);
                } else if(itemId == R.id.reports) {
                    dashboardViewSwitcher.setVisibility(View.INVISIBLE);
                }
            }
        });

        bottomBar.setActiveTabColor("#C2185B");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setup_options, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.log_out) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Log Out")
                    .setMessage("Are you sure you want to log out?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(AppMenu.this, MainMenu.class);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                         }
                    })
                    .show();
        }

        if (id == R.id.system_custom_lists) {
            Intent intent = new Intent(AppMenu.this, SystemCustomLists.class);
            startActivity(intent);
        }

        if (id == R.id.user_management) {
            Intent intent = new Intent(AppMenu.this, UserManagement.class);
            startActivity(intent);
        }

        if (id == R.id.company_settings) {
            Intent intent = new Intent(AppMenu.this, CompanySettings.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
