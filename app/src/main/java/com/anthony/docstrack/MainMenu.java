package com.anthony.docstrack;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainMenu extends Activity {

    EditText etEmail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        etEmail = (EditText) findViewById(R.id.editText);
        etPassword = (EditText) findViewById(R.id.editText2);
    }

    public void onLogin(View view) {
        // Get values of the edit texts
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        Log.i("emal", email);
        Log.i("emal", password);

        String type = "login";

        BackgroundWorker backgroundWorker = new BackgroundWorker(this);
        backgroundWorker.execute(type, email, password);
    }

    public void toSignUp(View view) {
        Intent intent = new Intent(MainMenu.this, AppMenu.class);
        startActivity(intent);
    }

    public void toForgotPassword(View view) {
        Intent intent = new Intent(MainMenu.this, ForgotPassword.class);
        startActivity(intent);
    }
    //C:\Users\Anthony\Documents\ITCS-USC\DocsTrack\Database\MAMP\htdocs
}
