package com.anthony.docstrack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

public class SystemCustomLists extends AppCompatActivity {

    ImageButton departmentsButton;
    ImageButton notificationsButton;
    ImageButton documentTypesButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_custom_lists);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        departmentsButton = (ImageButton) findViewById(R.id.departmentsButton);
        notificationsButton = (ImageButton) findViewById(R.id.notificationsButton);
        documentTypesButton = (ImageButton) findViewById(R.id.documentTypesButton);
    }

    public void customListEnter(View view) {
        Intent i = new Intent(SystemCustomLists.this, CustomListView.class);
        startActivity(i);
    }
}
